FROM debian:buster-slim

ENV DEBIAN_FRONTEND noninteractive

# Required packages
RUN apt-get update && apt-get --assume-yes install apt-utils && apt-get --assume-yes install apt-transport-https ca-certificates curl gnupg2 software-properties-common

# Install docker cli, gcloud sdk
RUN curl -sSL https://download.docker.com/linux/debian/gpg          | apt-key add -              && \
    add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian buster stable" && \
    curl -sSL https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -              && \
    add-apt-repository "deb https://packages.cloud.google.com/apt cloud-sdk main"                && \
    apt-get update                                                                               && \
    apt-get install --assume-yes docker-ce-cli google-cloud-sdk kubectl

# Install helm packages
ENV HELM2_URL https://get.helm.sh/helm-v2.16.1-linux-386.tar.gz
ENV HELM3_URL https://get.helm.sh/helm-v3.1.0-linux-386.tar.gz
RUN mkdir /helm-temp && cd /helm-temp               && \
    curl $HELM2_URL --output helm2.tgz              && \
    curl $HELM3_URL --output helm3.tgz              && \
    mkdir helm2 helm3                               && \
    tar xzf helm2.tgz -C helm2/                     && \
    tar xzf helm3.tgz -C helm3/                     && \
    mv helm2/linux-386/helm /usr/local/bin/helm2    && \
    mv helm3/linux-386/helm /usr/local/bin/helm3    && \
    cd / && rm -rf /helm-temp
